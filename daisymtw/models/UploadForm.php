<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }
    
    public function upload()
    {
        $filename = '';
        if ($this->validate()) {
            $filename = '/uploads/' . date('YmdHis').rand(100,999) . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs(Yii::$app->basePath.'/web'.$filename);
            return $filename;
        } else {
            return $filename;
        }
    }
}
