<?php

namespace app\models\db;

/**
 * This is the ActiveQuery class for [[Good]].
 *
 * @see Good
 */
class GoodQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Good[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Good|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
