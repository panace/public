<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "{{%goods_category}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property integer $parent_id
 * @property string $logo
 * @property integer $sort
 * @property string $describe
 */
class GoodsCategory extends Base
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%goods_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'sort'], 'integer'],
            [['name', 'alias', 'describe'], 'string', 'max' => 200],
            [['logo'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '分类中文名称'),
            'alias' => Yii::t('app', '分类英文名称'),
            'parent_id' => Yii::t('app', '所属分类'),
            'logo' => Yii::t('app', '分类logo'),
            'sort' => Yii::t('app', '排序'),
            'describe' => Yii::t('app', '商品分类描述'),
        ];
    }

    public function getTopArr()
    {
        $topCats = self::find()->where(['parent_id' => 0])->all();
        $arr = ['0' => '顶级分类']; 
        foreach($topCats as $cat){
            $arr[$cat['id']] =  $cat['name'];
        }
        return $arr;
    }

    /**
     * @inheritdoc
     * @return GoodsCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GoodsCategoryQuery(get_called_class());
    }
}
