<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "{{%good}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 * @property string $goods_no
 * @property string $unit
 * @property string $content
 * @property string $img
 * @property string $imgs
 * @property string $sell_price
 * @property string $market_price
 * @property integer $store_nums
 * @property string $desc
 * @property integer $weight
 * @property string $specs
 * @property string $attrs
 * @property integer $status
 * @property string $creationtime
 * @property string $updatetime
 * @property integer $deleted
 */
class Good extends Base
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%good}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['category_id', 'store_nums', 'weight', 'status', 'tag_id', 'deleted'], 'integer'],
            [['content', 'imgs', 'specs', 'attrs'], 'string'],
            [['sell_price', 'market_price'], 'number'],
            [['creationtime', 'updatetime','status','deleted','sell_price','market_price','weight','tag_id'], 'safe'],
            [['name', 'goods_no'], 'string', 'max' => 50],
            [['unit'], 'string', 'max' => 20],
            [['img', 'desc'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '商品名称'),
            'category_id' => Yii::t('app', '分类ID'),
            'goods_no' => Yii::t('app', '商品编号'),
            'unit' => Yii::t('app', '计量单位'),
            'content' => Yii::t('app', '详细介绍'),
            'img' => Yii::t('app', '默认图片'),
            'imgs' => Yii::t('app', '产品相册'),
            'sell_price' => Yii::t('app', '销售价'),
            'market_price' => Yii::t('app', '市场价'),
            'store_nums' => Yii::t('app', '库存'),
            'desc' => Yii::t('app', '商品简介'),
            'weight' => Yii::t('app', '重量(g)'),
            'specs' => Yii::t('app', '规格'),
            'attrs' => Yii::t('app', '产品属性'),
            'status' => Yii::t('app', '状态：0，下架；1 上架；'),
            'tag_id' => Yii::t('app', '标签ID'),
            'creationtime' => Yii::t('app', '创建时间'),
            'updatetime' => Yii::t('app', '更新时间'),
            'deleted' => Yii::t('app', '是否删除：0，未删除；1，已删除'),
        ];
    }

    public function beforeSave($insert)
    {
        $this->tag_id = $this->tag_id ? $this->tag_id : 0;
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     * @return GoodQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GoodQuery(get_called_class());
    }
}
