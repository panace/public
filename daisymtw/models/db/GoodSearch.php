<?php

namespace app\models\db;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\db\Good;

/**
 * GoodSearch represents the model behind the search form about `app\models\db\Good`.
 */
class GoodSearch extends Good
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'store_nums', 'weight', 'status', 'deleted'], 'integer'],
            [['name', 'goods_no', 'unit', 'content', 'img', 'imgs', 'desc', 'specs', 'attrs', 'creationtime', 'updatetime'], 'safe'],
            [['sell_price', 'market_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Good::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'sell_price' => $this->sell_price,
            'market_price' => $this->market_price,
            'store_nums' => $this->store_nums,
            'weight' => $this->weight,
            'status' => $this->status,
            'creationtime' => $this->creationtime,
            'updatetime' => $this->updatetime,
            'deleted' => $this->deleted,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'goods_no', $this->goods_no])
            ->andFilterWhere(['like', 'unit', $this->unit])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'img', $this->img])
            ->andFilterWhere(['like', 'imgs', $this->imgs])
            ->andFilterWhere(['like', 'desc', $this->desc])
            ->andFilterWhere(['like', 'specs', $this->specs])
            ->andFilterWhere(['like', 'attrs', $this->attrs]);

        return $dataProvider;
    }
}
