<?php

namespace app\models\db;

use Yii;

class Base extends \yii\db\ActiveRecord
{
   	public function beforeSave($insert)
    {
        if($insert){
        	if(property_exists($this, 'creationtime')){
        		$this->creationtime = $this->creationtime ? $this->creationtime : date('Y-m-d H:i:s');
        	}
            
        }
        if(property_exists($this, 'updatetime')){
        	$this->updatetime = $this->updatetime ? $this->updatetime : date('Y-m-d H:i:s');
        }
       if(property_exists($this, 'deleted')){
        	$this->deleted = $this->deleted ? $this->deleted : 0;
        }
        return parent::beforeSave($insert);
    }
}
