<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $uid
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $mobile
 * @property string $mail
 * @property string $qq
 * @property string $logo
 * @property string $lastlogintime
 * @property string $creationtime
 * @property string $updatetime
 * @property integer $deleted
 */
class User extends Base
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'name', 'mobile', 'mail'], 'required'],
            [['lastlogintime', 'creationtime', 'updatetime'], 'safe'],
            [['deleted'], 'integer'],
            [['username', 'name', 'mobile', 'qq'], 'string', 'max' => 20],
            [['password', 'mail', 'logo'], 'string', 'max' => 64],
            [['username'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => Yii::t('app', '用户ID'),
            'username' => Yii::t('app', '用户名'),
            'password' => Yii::t('app', '密码'),
            'name' => Yii::t('app', '昵称'),
            'mobile' => Yii::t('app', '手机'),
            'mail' => Yii::t('app', '邮箱'),
            'qq' => Yii::t('app', 'QQ号码'),
            'logo' => Yii::t('app', '头像'),
            'lastlogintime' => Yii::t('app', '最后登录时间'),
            'creationtime' => Yii::t('app', '创建时间'),
            'updatetime' => Yii::t('app', '更新时间'),
            'deleted' => Yii::t('app', '已删除'),
        ];
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }
}
