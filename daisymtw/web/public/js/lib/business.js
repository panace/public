/*
version: 1.0
author: allen cj
email: 309558639@qq.com
*/
define(function(require, exports, module) {
	var $ = require("jquery");
	var Public = require("common");
	var Dialog = require('dialog');
	var Business = Business || {}; //业务组件
	/*
	 * 双向选择
	*/
	Business.multipleBox = function(opts){
        opts.$obj.click(function(e){
            e.preventDefault();
            var $this = $(this);
            var list = $this.data('list');

            if(list) {
            	init(list);
            } else {
	            Public.get({
	            	position: 'right',
	                url: SYSTEM.appUrl + 'SchoolOrg/getList',
	                data: { schoolId: SYSTEM.schoolId, pageSize: 200 },
	                success: function(data) {
	                	init(data.data.orgList);
	                	$this.data('list', data.data.orgList);
	                }
	            });
            };

            function init(data){
	            opts.mode({
	                title: '添加组织机构',
	                content: '<div id="multipleBox"></div>',
	                okValue: '确 定',
	                onshow: function(){
	                    $('#multipleBox').multipleBox({data: data});                     
	                },
	                ok: function () {
	                    var $options = $('#multipleBox').find('.selected');
	                    if($options.length > 0) {
	                    	var curIds = $('#frTable').data('cur') || [];
	                    	var addIds = $('#frTable').data('add') || [];
	                        var ids = [];
	                        var domStr = '';
	                        $options.each(function (index, domEle) { 
	                            // domEle == this 
	                            var $this = $(this);
	                            var orgId = $this.data('id');
	                            var orgName = $this.find('.name').text();
	                            var orgTypeName = $this.data('type');
	                            var gradeNames = $this.data('grade');
	                            var lessonNames = $this.data('lesson');
	                            if($.inArray(orgId, curIds) !== -1) {
	                            	return false;
	                            };
	                            ids.push(orgId);
	                            domStr += '<tr id="rtA' + orgId + '">';
	                            domStr += 		'<td class="tl">' + orgName + '</td>';
	                            domStr += 		'<td>' + orgTypeName + '</td>';
	                            domStr += 		'<td class="tl">' + gradeNames + '</td>';
	                            domStr += 		'<td>' + lessonNames + '</td>';
	                            domStr += 		'<td><a class="delete" data-id="1001">解除关联</a></td>';
	                            domStr += '</tr>';
	                        });
	                        $('#frTable').data('cur', ids.concat(curIds));
	                        $('#frTable').data('add', ids.concat(addIds));
	                        $('#frTable').find('tbody').prepend(domStr);
	                    } else {
	                        Public.tips({ dialog: true, type: 1, content : '未选择组织机构！' });
	                        return false;
	                    }
	                },
	                cancelValue: '取消',
	                cancel: function () {
	                }
	            }).showModal();
            };
        });
	};

	/**
	 * 省市区
	 * 省下拉框ID：provinceCombo
	 * 市下拉框ID：cityCombo
	 * 区下拉框ID：areaCombo
	 * 需要搭配COMBO插件
	 * 默认值放在data('defaultValue')
	 */	
	Business.regionLinkage = function(config) {
		var	_provinceList = [],
			_cityList = [], 
			_areasList = [],
			_pCombo, 
			_cCombo, 
			_aCombo, 
			curRegion,
			curProvince,
			curCity,
			curArea,
			mod = {};

		var defaults = {
			remind: false,
	        $provinceCombo: $('#provinceCombo'),
	        $cityCombo: $('#cityCombo'),
	        $areaCombo: $('#areaCombo')
		}; 

		if(window.sessionStorage) {
			curRegion = JSON.parse(sessionStorage.getItem('curRegion')) || {};
			curProvince	= curRegion.provinceCode;
			curCity	= curRegion.cityCode;
			curArea	= curRegion.areaCode;
		};

		var opts = $.extend({}, defaults, config);  

		function init(_provinceCombo, _cityCombo, _areaCombo, callback){
			_pCombo = _provinceCombo;
			_cCombo = _cityCombo;
			_aCombo = _areaCombo;
			//缓存省市数据
			if(!(_provinceCombo&&_cityCombo&&_areaCombo)){
				return;
			}
			mod.provinceCombo = _getCombo( _pCombo, _getProvinceData());
			mod.cityCombo = _getCityCombo( _cCombo,[]);
			mod.areaCombo = _getAreaCombo( _aCombo,[]);

			if(curProvince) {
				mod.provinceCombo.selectByValue(curRegion.provinceCode, true);
				curProvince = '';
			} else {
				mod.provinceCombo.selectByIndex(0, true);
			};

			return mod;
		};
		function _getCombo(obj, data){
			var _disabled = $(obj).hasClass('disabled');
			var combo = obj.combo({
				data: data,
				text: 'mc',
				value: 'dm',
				width: 149,
				defaultSelected: -1,
				defaultFlag: false,
				//addOptions: {text:'', value: -1},
				disabled: _disabled,
				callback: {
					onChange: function(){
						if(curCity) {
							mod.cityCombo.loadData(_getCityData(mod.provinceCombo.getValue()), ["dm", curCity], true);
							curCity = '';
						} else {
							mod.cityCombo.loadData(_getCityData(mod.provinceCombo.getValue()), 0, true);
						};
						//mod.cityCombo.loadData(_getCityData(mod.provinceCombo.getValue()), 0, true);
						//mod.areaCombo.loadData([],-1,false);
					}
				}
			}).getCombo();
			$(obj).data('combo', combo);
			return combo;
		};
		function _getCityCombo(obj, data){
			var _disabled = $(obj).hasClass('disabled');
			var combo = obj.combo({
				data: data,
				text: 'mc',
				value: 'dm',
				width: 'auto',
				minWidth: 149,
				defaultSelected: -1,
				//addOptions: {text:'', value: -1},
				disabled: _disabled,
				callback: {
					onChange: function(){
						if(curArea) {
							mod.areaCombo.loadData(_getAreaData(mod.cityCombo.getValue()), ["dm", curRegion.areaCode], true);
							curArea = '';
						} else {
							mod.areaCombo.loadData(_getAreaData(mod.cityCombo.getValue()), 0, true);
						};
						//mod.areaCombo.loadData(_getAreaData(mod.cityCombo.getValue()), 0, true);
					}
				}
			}).getCombo();
			$(obj).data('combo', combo);
			return combo;
		};
		function _getAreaCombo(obj, data){
			var _disabled = $(obj).hasClass('disabled');
			var combo = obj.combo({
				data: data,
				text: 'mc',
				value: 'dm',
				width: 'auto',
				minWidth: 149,
				defaultSelected: -1,
				//addOptions: {text:'', value: -1},
				disabled: _disabled,
				callback: {
					onChange: function(){
						opts.callback && opts.callback(this.getValue());
					}
				}
			}).getCombo();
			$(obj).data('combo', combo);
			return combo;
		};
		function _getProvinceData (){
			var _data = [];
	        $.each(SYSTEM.areaList.provinceList, function(key, val){
	        	_data.push(val);      
	        });
	        if(opts.remind) {
	        	return $.merge([{mc: '选择省', dm: -1}], _data);
	        } else {
	        	return _data;
	        };
		};
		function _getCityData (PID){
			var _data = [];
	        $.each(SYSTEM.areaList.cityList, function(key, val){
	            if(val.parent == PID) {
	                _data.push(val);
	            };                                    
	        });        
	        if(opts.remind) {
	        	return $.merge([{mc: '选择市', dm: -1}], _data);
	        } else {
	        	return _data;
	        };
		};
		function _getAreaData (PID){
			var _data = [];
	        $.each(SYSTEM.areaList.regionList, function(key, val){
	            if(val.parent == PID) {
	                _data.push(val);
	            };                                    
	        });
	        if(opts.remind) {
	        	return $.merge([{mc: '选择区', dm: -1}], _data);
	        } else {
	        	return _data;
	        };
		};

		if(SYSTEM.areaList) {
			init(opts.$provinceCombo, opts.$cityCombo, opts.$areaCombo, opts.callBack);
			//console.log(mod)
			return mod;
		} else {
			$.getJSON(SYSTEM.appUrl + "Common/getAreaList?platfrom=" + SYSTEM.platform, function(data){
				SYSTEM.areaList = data.data;
				init(opts.$provinceCombo, opts.$cityCombo, opts.$areaCombo, opts.callBack);
				console.log(mod)
				return mod;
			});
		};
	};

	Business.addSubject = function(opts) {
        opts.$dom.find('.ui-opt-custom').click(function(){
            opts.dialog({
                width: 400,
                title: '新增科目',
                content: opts.view.subjectDom,
                okValue: '确 定',
                ok: function () {
                    var d = this;
                    var lessonName = $('#fdName').val();
                    if(!lessonName) {
                        Public.tips({ dialog: true, type: 1, content : '科目名称不能为空！' });
                        return false;
                    };
                    var simpleName = $('#fdAbbr').val();
                    if(!simpleName) {
                        Public.tips({ dialog: true, type: 1, content : '科目简称不能为空！' });
                        return false;
                    };
                    opts.model.createSubject({
                        data: { schoolId: SYSTEM.schoolId, name: lessonName, simpleName: simpleName },
                        callback: function(data){
                            opts.$dom.find('.ui-opt-custom').before('<span class="ui-opt" data-id="' + data.id + '">' + data.name + '</span>');
                            d.close().remove();
                            if(opts.position === 'Dialog') {
                            	Public.tips({ dialog: true, content : '新增成功！' });
                            } else {
                            	Public.tips({ right: true, content : '新增成功！' });
                            };
                            
				            //更新最新科目
				            opts.model.getSubjectList({
				                schoolId: SYSTEM.schoolId
				            });
                        }
                    });
                    return false;
                },
                cancelValue: '取消',
                cancel: function () {}
            }).showModal();

        });
	};

	/*
	 * 生效范围
	*/
	Business.scopeChoose = function(opts){
        $('#fScope').click(function(e){
            e.preventDefault();
            var $original = $(this);
            opts.mode({
                title: '选择地区范围',
                content: '<div class="area-box" id="areaBox"></div>',
                okValue: '确 定',
                onshow: function(){
                    var original = $original.data('original');
                    if(original) {
                        $('#areaBox').areaBox({value: original});
                    } else {
                        $('#areaBox').areaBox();
                    };                   
                },
                ok: function () {
                    var $options = $('.result-section').find('.options');
                    if($options.length > 0) {
                        var data = [];
                        var str = '<ul>';
                        $options.each(function (index, domEle) { 
                            // domEle == this 
                            var $this = $(this);
                            var type = $this.data('type');
                            var key = $this.data('key');
                            var name = $this.find('.name').text();
                            data.push({ type: type, id: key, name: name });
                            str += '<li><del data-type="' + type + '" data-id="' + key + '">移除</del><span>' + name + '</span></li>';
                        });
                        $original.data('original', data);
                        str += '</ul>';
                        if($options.length > 0) {
                            $('.fselectArea').html(str).show();
                        } else {
                            $('.fselectArea').empty().hide();
                        }; 
                    } else {
                        $original.removeData('original').removeClass('active').text('选择地区...');
                    }
                },
                cancelValue: '取消',
                cancel: function () {
                }
            }).showModal();
        });
	};

	/*
	 * 日期范围选择
	*/
	Business.dateRange = function(opts) {
		var defaults = {
            language:'cn',
            separator : ' ~ ',
            autoClose: true
		};
		var config = $.extend({}, defaults, opts);

        config.$obj.dateRangePicker(config);
	};

	/*
	 * 分页组件
	 */
	Business.pageBox = function(opts){
		var _page = opts._page,
			data = opts.data,
			lineHeight = opts.lineHeight || 37;
		_page.totalNumber = data.data.totalNumber;
        _page.$queryResult.text(data.data.totalNumber);
        originData = data.data;
        _page.gridData = data.data;
        var menuWidth = SYSTEM.hasMenu == 0 ? 0 : 160; 
        var _width = $(window).width() - menuWidth - 30 - 2;
        var _height = $(window).height() - _page.offset.top - 3 - 34;
        var maxRow = Math.floor(_height / lineHeight);
        if(originData.totalPage > 1) {
            _height = _height - 52;
            if(20 > maxRow) {
                _page.$grid.jqGrid('setGridHeight', _height);
                _page.$grid.jqGrid('setGridWidth', _width);
            } else {
                _page.$grid.jqGrid('setGridHeight', 'auto');
                _page.$grid.jqGrid('setGridWidth', _width);
            };
            _page.$pager.show();
            _page.diff = 52;
        } else {
            if(originData.totalNumber > maxRow) {
                _page.$grid.jqGrid('setGridHeight', _height);
                _page.$grid.jqGrid('setGridWidth', _width);
            } else {
                _page.$grid.jqGrid('setGridHeight', 'auto');
                _page.$grid.jqGrid('setGridWidth', _width);
            };
            _page.$pager.hide();
            _page.diff = 0;
        };

        //分页
        if(!_page.instance) {
            if(data.total === 0) {
                _page.$pager.empty().html('<p>无分页信息</p>');
            } else {
                _page.$pager.empty().pagination(originData.totalNumber, {
                    num_edge_entries: 2,
                    num_display_entries: 8,
                    callback: pageselectCallback,
                    items_per_page: 20 
                });
            };
        };
        _page.instance = true;

        //分页回调
        function pageselectCallback(page_index, jq){
            Public.loading.show();
            _page.$container.find(".ui-jqgrid-bdiv").scrollTop(0);
            _page.$grid.jqGrid('setGridParam', {page: page_index + 1}).trigger("reloadGrid");
            return false;
        };
	};
	
	/*
	 * 图片上传
	*/
	Business.uploader = function(opts){
		var defaults = {
			type: 'gif,jpg,jpeg,png',
			cut: true
		};     
		var config = $.extend({}, defaults, opts);
		console.log(config)
	    var _page = config.origin;
	    var $progress = $(".progressbar");
	    var uploader = WebUploader.create({
	    
	        // 选完文件后，是否自动上传。
	        auto: true,

	        compress: false,
	        //为允许上传重复文件
	        duplicate :true,
	    
	        // swf文件路径
	        swf: '/static/resource-ad/js/plugin/Uploader.swf',
	    
	        // 文件接收服务端
	        server: config.server,
	    
	        // 选择文件的按钮。可选。
	        // 内部根据当前运行是创建，可能是input元素，也可能是flash.
	        pick: '#filePicker',
	        fileSingleSizeLimit : 2097152
	    
	        // 只允许选择图片文件。
	        // accept: {
	        //     title: 'Images',
	        //     extensions: config.type,
	        //     mimeTypes: 'image/*'
	        // }
	    });

	    // 当有文件被添加进队列的时候
	    uploader.on( 'fileQueued', function( file ) {
	        $progress.show();
	    });
	    // 文件上传过程中创建进度条实时显示。
	    uploader.on( 'uploadProgress', function( file, percentage ) {
	        //console.log(percentage * 100 + '%')
	        console.log(percentage)
	        $progress.css({'width':percentage * 100 + '%'});
	        $('#uploader').find('.webuploader-pick').text('正在上传...');
	    });

	    uploader.on( 'uploadSuccess', function(file, data) {
	        if(data.code === 0){
	            var state = data.data.state;
	            if(config.cut) {

	            } else {
                    $('#imgUrl').attr('src', data.data.imgUrl);
                    $('#fileWrap').addClass('uploaded').find('.webuploader-pick').text('重新上传');
                    Public.tips({ right:true, content: '图片上传成功！' });
	            };
	        } else {
	            Public.tips({ type: 1, right:true, content: data.msg });
	            $('#fileWrap').find('.webuploader-pick').text('重新上传');
	        }
	    });

	    uploader.on( 'uploadComplete', function( file ) {
	        //debugger;
	        $progress.hide(500,function(){
	            $(this).css({'width':0});
	        });
	    });

	    uploader.on( 'uploadError', function( file ) {
	        Public.tips({ right:true, type:1, content: '图片上传失败！' });
	    });

	    uploader.on('error', function(e){
	        if(e === 'Q_TYPE_DENIED'){
	            Public.tips({ right:true, type:2, content: '上传文件类型不符！' });
	        };
	        if(e === 'F_EXCEED_SIZE'){
	            Public.tips({ right:true, type:2, content: '上传图片文件大小不能超过2M！' });
	        };

	    });
	};

	window.Business = Business;

	module.exports = Business;
});