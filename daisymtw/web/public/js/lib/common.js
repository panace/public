/*
version: 1.0
author: allen cj
email: 309558639@qq.com
*/
define(function(require, exports, module) {
	var $ = require("jquery");
	var Public = Public || {}; //公用方法
	//浏览器版本提示
	(function(){
		if(navigator.appName === "Microsoft Internet Explorer"){
			var browser = navigator.appName;
			var b_version = navigator.appVersion;
			var version = b_version.split(";");
			if(version === "MSIE 6.0" || version === "MSIE 7.0" || version === "MSIE 8.0") {
				$('body').append('<div id="compatibleBack">'+
								'    <div id="compatibleBlock">'+
								'        <div class="l broser-tip">'+
								'            <h5>亲爱的云校园用户：</h5>'+
								'            <p style="text-indent: 2em;">您的浏览器版本过低，云校园部分功能可能无法正常使用。为了您更好地使用云校园，建议您对当前浏览器进行升级，或使用其他浏览器进行访问。推荐浏览器：</p>'+
								'        </div>'+
								'        <ul class="r broser">'+
								'            <li><a href="http://www.google.cn/chrome/browser/desktop/index.html" class="chrome">Chrome</a></li>'+
								'            <li><a href="http://browser.qq.com/" class="qq">QQ浏览器</a></li>'+
								'            <li><a href="http://se.360.cn/" class="zh360">360浏览器</a></li>'+
								'        </ul>'+
								'        <a id="compatibleClose"></a>'+
								'    </div>'+
								'</div>').show();
				$('#compatibleClose').click(function(){
					$('#compatibleBack').slideUp();
				});
			};
		};
	})();
	//通用下拉
	(function(){
		$('.dropdown').hover(function() {
			$(this).addClass("dropdown-open");
		}, function() {
			$(this).removeClass("dropdown-open");
		});
	})();

	//通用post请求，用于提交数据
	Public.post = function(opts) {
		var btnText = opts.btn ? opts.btn.val() : '';	//原始按钮内容
		opts.position = opts.position || 'center';	//提示
		$.ajax({
			type: "POST",
			url: opts.url,
			data: opts.data || {},
			dataType: "json",
			cache: false,
			beforeSend: function(xhr, settings) {
				opts.btn && opts.btn.addClass('disabled').val('处理中...');
				opts.loading !== false ? Public.loading.show({ position: opts.position }) : '';
			},
			success: function(data, status) {
				opts.loading !== false ? Public.loading.hide() : '';
				if (data.code === 0) {
					opts.success && opts.success(data);
					return;
				} else if (data.code === 1001) {
					Public.tips({
						position: opts.position,
						type: 1,
						content: '会话已过期，请重新登录！'
					});
					setTimeout(function() {
						window.location.reload();
					}, 10);
				} else if (data.code === 403) {
					Public.tips({
						position: opts.position,
						type: 1,
						content: 'Sorry，您没有相关操作权限！'
					});
				}else if (data.code === 1017) {
					Public.tips({
						position: opts.position,
						type: 1,
						content: 'Sorry，您的权限不足！'
					});
					setTimeout(function() {
						window.location.href = SYSTEM.baseUrl;
					}, 100);
				} else {
					Public.tips({
						position: opts.position,
						type: 1,
						content: data.msg
					});
				};
				opts.btn && opts.btn.removeClass('disabled').val(btnText);
			},
			error: function(xhr, type) {
				opts.loading !== false ? Public.loading.hide() : '';
				Public.tips({
					position: opts.position,
					type: 1,
					content: '请求失败，请联系管理员或稍后再试！'
				});
				opts.error && opts.error();
				opts.btn && opts.btn.removeClass('disabled').val(btnText);
			}
		});
	};

	//通用get请求，用于获取数据
	Public.get = function(opts) {
		opts.position = opts.position || 'center';
		$.ajax({
			type: "GET",
			url: opts.url,
			data: opts.data || {},
			dataType: opts.dataType || "json",
			cache: false,
			beforeSend: function(xhr, settings) {
				opts.loading !== false ? Public.loading.show({ position: opts.position }) : '';
			},
			success: function(data, status) {
				opts.loading !== false ? Public.loading.hide() : '';
				if (data.code === 0) {
					opts.success && opts.success(data);
				} else if (data.code === 1001) {
					Public.tips({
						position: opts.position,
						type: 1,
						content: '会话已过期，请重新登录！'
					});
					setTimeout(function() {
						window.location.reload();
					}, 10);
				} else if (data.code === 403) {
					Public.tips({
						position: opts.position,
						type: 1,
						content: 'Sorry，您没有相关操作权限！'
					});
				}else if (data.code === 1017) {
					Public.tips({
						position: opts.position,
						type: 1,
						content: 'Sorry，您的权限不足！'
					});
					setTimeout(function() {
						window.location.href = SYSTEM.baseUrl;
					}, 100);
				} else {
					Public.tips({
						position: opts.position,
						type: 1,
						content: data.msg
					});
				};
			},
			error: function(xhr, type) {
				opts.loading !== false ? Public.loading.hide() : '';
				Public.tips({
					position: opts.position,
					type: 1,
					content: '请求失败，请联系管理员或稍后再试！'
				});
				opts.error && opts.error();
			}
		});
	};

	//设置表格宽高
	Public.setGrid = function(opts){
		var winW = $(window).width();
		winW = winW >= 1260 ? winW : 1260;
		var menuWidth = SYSTEM.hasMenu == 0 ? 0 : 160;
		var gridW = winW - menuWidth - 30 - 2, gridH = $(window).height() - opts.top - 15 - 35;
		return {
			width : gridW,
			height : gridH
		}
	};

	//重设表格宽高
	Public.resizeGrid = function(opts){
		console.log(opts)
		var _page = opts.page;
	    var data = _page.gridData;
	    var rowNum = _page.$grid.jqGrid('getGridParam','rowNum');
	    var menuWidth = SYSTEM.hasMenu == 0 ? 0 : 160;
	    var min_width = $(window).width();
	    if(min_width < 1260) {
	    	min_width = 1260;
	    };
	    var _width = min_width - menuWidth - 30 - 2;
	    var _height = $(window).height() - _page.offset.top - 3 - 34;
	    var maxRow = Math.floor(_height / 37);
	    if(!data){return;}
	    if(data.totalPage > 1) {
	        _page.diff = 52;
	        _height = _height - 52;
	        if(rowNum > maxRow) {
	            _page.$grid.jqGrid('setGridHeight', _height);
	            _page.$grid.jqGrid('setGridWidth', _width);
	        } else {
	            _page.$grid.jqGrid('setGridHeight', 'auto');
	            _page.$grid.jqGrid('setGridWidth', _width);
	        };
	        _page.$pager.show();
	    } else {
	        _page.diff = 0;
	        if(data.totalNumber > maxRow) {
	            _page.$grid.jqGrid('setGridHeight', _height);
	            _page.$grid.jqGrid('setGridWidth', _width);
	        } else {
	            _page.$grid.jqGrid('setGridHeight', 'auto');
	            _page.$grid.jqGrid('setGridWidth', _width);
	        };
	        _page.$pager.hide();
	    };
	};

	/**
	 * 	右滑层
	 */
	function RightBox(opts){
        this.option = $.extend({}, {
            //内容类型:str-字符串; tpl-模版
            //填充类型为str时, innerContentHtml为要填充的字符串
            //填充类型为模版时, innerContentHtml为模版地址
            innerContentType: "tpl",
            innerContentValue: '',
            mainBoxClssName: "",	//滑层Class
            mainBoxID: undefined,	//滑层ID
            //宽度设置: 1、全屏; 2、百分比; 3、固定值宽度; 4、宽度临界到菜单
            //模式为2时, widthValue为百分比值(0-100);
            //模式为3时, widthValue为具体像素值
            widthType: 3,
            widthValue: 960,
            afterInner: $.noop,		//插入内容后的回调
            beforeInitEvent:null,
            beforeShow: null,
            afterShow: null,
            beforeClose: null,
            afterClose: null
        }, opts);

		this.init();
	};

	RightBox.prototype = {
		constructor: RightBox,
		init: function(){
			var _this = this;
			var _option = this.option;
		    var wrapHMTL = '<div class="rightbox-wrap"></div>',
		    	contentHTML = '';
		    contentHTML += '<div class="rightbox-black"></div>';
		    contentHTML += '<div class="rightbox-main">';
		    contentHTML += 		'<div class="rightbox">';
		    contentHTML += 			'<div class="rightbox-close"></div>';
		    contentHTML += 			'<div class="rightbox-content ui-scroll">';
		    contentHTML += 				'<div class="inner-rightbox-content">';
		    contentHTML += 				'</div>';
		    contentHTML += 			'</div>';
		    contentHTML += 		'</div>';
		    contentHTML += '</div>';

		    this.$el = $(wrapHMTL).appendTo('body');
	        this.$el.html(contentHTML);

	        if(_option.innerContentType == "tpl") {
	        	//获取模板文件
	            $.get(_option.innerContentValue, function(data) {
	                _this.$el.find('.inner-rightbox-content').empty().append(data);
	        		_this.widthRender();
	        		_option.afterInner();
	        		_this.showAnimat();
	        		_this.initEvent();
	            });
	        } else {
	            this.$el.find('.inner-rightbox-content').empty().append(_option.innerContentValue);
		        this.widthRender();
		        _option.afterInner();
		        this.showAnimat();
		        this.initEvent();
	        }
	    },
		widthRender: function (){
	        var _this = this,
	            widthStr;

	        switch(_this.option.widthType){
	            case 1:
	                widthStr = "100%;";
	                break;
	            case 2:
	                widthStr = _this.option.widthValue +'%;';
	                break;
	            case 3:
	                widthStr = (_this.option.widthValue + _this.$el.find('.rightbox-close').width())+'px;';
	                break;
	            case 4:
	                var w = window.screen.availWidth,
	                    l = $('.layout-body').width(),
	                    n = $('.layout-nav').width();
	                var	b = (w - l) / 2;
	                var autoWidth = w - b - n + _this.$el.find('.rightbox-close').width();
	                widthStr = autoWidth +'px;';
	                break;
	            default:
	        };
	        _this.$el.find('.rightbox').attr('style', ['right:',"-" + widthStr, 'px;','width:',widthStr,';'].join(''));
	        if (document.documentElement.clientHeight < document.documentElement.offsetHeight) {
	        	$('html').addClass('init-rightbox');
	        };
	    },
		showAnimat: function (){
	        var _this = this;
	        typeof _this.option.beforeShow === "function" && _this.option.beforeShow(_this.$el);
	        _this.selfWidth = _this.$el.find('.rightbox').width();
	        _this.$el.find('.rightbox-black').css("opacity","0").animate({ "opacity":.76 }, 100, function (){
	            _this.$el.find('.rightbox').animate({ "right": 0 }, 300, function(){});
	            typeof _this.option.afterShow === "function" && _this.option.afterShow(_this.$el);
	        });
	    },
	    initEvent: function (){
	        var _this = this;

	        typeof _this.option.beforeInitEvent === "function" && _this.option.beforeInitEvent(_this.$el);

	        _this.$el.undelegate("click").delegate('.rightbox-close', "click", function (){
		        if(typeof _this.option.beforeClose === "function") {
		            var close = _this.option.beforeClose(_this.$el);
		            if(close === false) {
		            	return false;
		            } else {
		            	_this.destroy();
		            }
		        } else {
		        	_this.destroy();
		        };
	        });
	    },
		destroy: function(){
	        this.$el.empty().remove();
	        $('html').removeClass('init-rightbox');
	        typeof this.option.afterClose === "function" && this.option.afterClose(this.$el);
		}
	}

	Public.rightBox = function(opts){
		return new RightBox(opts);
	}

	//根据URL获取参数值
	Public.urlParam = function() {
		var param, url = location.hash,
			theRequest = {};
		if (url.indexOf("?") != -1) {
			var str = url.split('?')[1];
			strs = str.split("&");
			for (var i = 0, len = strs.length; i < len; i++) {
				param = strs[i].split("=");
				theRequest[param[0]] = decodeURIComponent(param[1]);
			}
		}
		return theRequest;
	};

	//加载中...
	Public.loading = {
		show: function(opts) {
			var defaults = {
				position: 'center',
				content: '努力加载中...'
			};
			var opts = $.extend({}, defaults, opts);

			if($('#loadingWrap').length < 1) {
				$('body').append('<div class="mask dn" id="loadingWrap"><div class="ui-loading"><i></i></div></div>');
			};
			var $loading = $('#loadingWrap .ui-loading');
			if(opts.right || opts.position === 'right') {
				var rightW = $('.rightbox-content').outerWidth();
				$loading.css({
					marginLeft:  rightW/2 - $loading.outerWidth()/2
				});
			} else {
				$loading.css({
					marginLeft: - $loading.outerWidth()/2
				});
			};
			$('#loadingWrap').show();
		},
		hide: function() {
			$('#loadingWrap').hide();
		}
	};

	/*
	 *公共提示
	 *示例：Public.tips({type: 1, content : data.msg});
	 */
	Public.tips = function(options){ return new Public.Tips(options); }
	Public.Tips = function(options){
		var defaults = {
			renderTo: 'body',
			right: false,
			type : 0,
			autoClose : true,
			removeOthers : true,
			time : undefined,
			top : 50,
			onClose : null,
			onShow : null
		}
		this.options = $.extend({},defaults,options);
		if(this.options.position === 'dialog') {
			this.options.dialog = true;
		} else if(this.options.position === 'right'){
			this.options.right = true;
		};
		if(this.options.dialog === true) {
			this.options.renderTo = '.ui-dialog'
		};
		this._init();

		!Public.Tips._collection ?  Public.Tips._collection = [this] : Public.Tips._collection.push(this);

	}

	Public.Tips.removeAll = function(){
		try {
			for(var i=Public.Tips._collection.length-1; i>=0; i--){
				Public.Tips._collection[i].remove();
			}
		}catch(e){}
	}

	Public.Tips.prototype = {
		_init : function(){
			var self = this,opts = this.options,time;
			if(opts.removeOthers){
				Public.Tips.removeAll();
			}

			this._create();

			if(opts.autoClose){
				time = opts.time || opts.type == 1 ? 2000 : 2000;
				window.setTimeout(function(){
					self.remove();
				},time);
			}

		},

		_create : function(){
			var opts = this.options, self = this;
			if(opts.autoClose) {
				this.obj = $('<div class="ui-tips"><i></i></div>').append(opts.content);
			} else {
				this.obj = $('<div class="ui-tips"><i></i><span class="close"></span></div>').append(opts.content);
				this.closeBtn = this.obj.find('.close');
				this.closeBtn.bind('click',function(){
					self.remove();
				});
			};

			switch(opts.type){
				case 0 :
					this.obj.addClass('ui-tips-success');
					break ;
				case 1 :
					this.obj.addClass('ui-tips-error');
					break ;
				case 2 :
					this.obj.addClass('ui-tips-warning');
					break ;
				default :
					this.obj.addClass('ui-tips-success');
					break ;
			}

			this.obj.appendTo(opts.renderTo).hide();
			this._setPos();
			if(opts.onShow){
				opts.onShow();
			}

		},

		_setPos : function(){
			var self = this, opts = this.options;
			if(opts.width){
				this.obj.css('width',opts.width);
			}
			var h =  this.obj.outerHeight(),winH = $(window).height(),scrollTop = $(window).scrollTop();
			//var top = parseInt(opts.top) ? (parseInt(opts.top) + scrollTop) : (winH > h ? scrollTop+(winH - h)/2 : scrollTop);
			var top = parseInt(opts.top);
			this.obj.css({
				position : Public.isIE6 ? 'absolute' : 'fixed',
				left : '50%',
				top : top,
				zIndex : '9999',
				marginLeft : -self.obj.outerWidth()/2
			});

			if(opts.right) {
				var rightW = $('.rightbox-content').outerWidth();
				var blackW = $(window).width() - rightW;
				this.obj.css({
					top : 24,
					left: 0,
					marginLeft :  blackW + rightW/2 - self.obj.outerWidth()/2
				});
			};

			if(opts.dialog) {
				this.obj.css({
					position: 'absolute',
					top : 12
				});
			};

			self.obj.show();

			if(Public.isIE6){
				$(window).bind('resize scroll',function(){
					var top = $(window).scrollTop() + parseInt(opts.top);
					self.obj.css('top',top);
				})
			}
		},

		remove : function(){
			var opts = this.options;
			this.obj.fadeOut(200,function(){
				$(this).remove();
				if(opts.onClose){
					opts.onClose();
				}
			});
		}
	};

	/*
	 * 日期范围选择
	*/
	Public.dateRange = function(opts) {
		var defaults = {
            language:'cn',
            separator : ' ~ ',
            autoClose: true
		};
		var opts = $.extend({}, defaults, opts);

        opts.$obj.dateRangePicker(opts);
	};

	Public.rules = {
	    digits: [/^\d+$/, "请填写数字"],
	    letters: [/^[a-z]+$/i, "请填写字母"], //纯字母
	    date: [/^\d{4}-\d{2}-\d{2}$/, "请填写有效的日期，格式:yyyy-mm-dd"],
	    time: [/^([01]\d|2[0-3])(:[0-5]\d){1,2}$/, "请填写有效的时间，00:00到23:59之间"],
	    email:[/^[\w\+\-]+(\.[\w\+\-]+)*@[a-z\d\-]+(\.[a-z\d\-]+)*\.([a-z]{2,4})$/i, '请填写有效的邮箱'],
	    url: [/^(https?|s?ftp):\/\/\S+$/i, "请填写有效的网址"],
	    qq: [/^[1-9]\d{4,}$/,"请填写有效的QQ号"],
	    IDcard: [/^\d{6}(19|2\d)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)?$/, "请填写正确的身份证号码"],
	    tel: [/^(?:(?:0\d{2,3}[\- ]?[1-9]\d{6,7})|(?:[48]00[\- ]?[1-9]\d{6}))$/, "请填写有效的电话号码"],  //办公或家庭电话
	    mobile: [/^1[3-9]\d{9}$/, "请填写有效的手机号"],  //移动电话
	    zipcode: [/^\d{6}$/, "请检查邮政编码格式"],
	    chinese: [/^[\u0391-\uFFE5]+$/, "请填写中文字符"],
	    username: [/^\w{3,12}$/, "请填写3-12位数字、字母、下划线"], //用户名
	    password: [/^[\S]{6,16}$/, "请填写6-16位字符，不能包含空格"], //密码
	    //可接受的后缀名，例如：accept(png|jpg|bmp|gif);
	    accept: function(element, params){
	        if (!params) return true;
	        var ext = params[0],
	            value = $(element).val();
	        return (ext === '*') ||
	               (new RegExp(".(?:" + ext + ")$", "i")).test(value) ||
	               this.renderMsg("只接受{1}后缀的文件", ext.replace(/\|/g, ','));
	    }
	};

	window.Public = Public;

	module.exports = Public;

	//通用选项插件
	;(function(){
		$.fn.selectBox = function(opts){
			var defaults = {
				data: null,
				isAll: false,
				type: 'radio',
				extend: false
			};
			var opts = $.extend(defaults, opts);
			var $this = $(this);
			var htmlStr = '';
			if(opts.data !== null) {
				if(opts.isAll) {
					opts.type = 'checkbox';
					htmlStr += '<lable class="ui-opt ui-opt-all">全部</lable>';
				};
				$.each(opts.data, function(i, val) {
					htmlStr += '<span class="ui-opt" data-id="' + val.id + '">' + val.name + '</span>';
				});

				if(opts.extend) {
					htmlStr += '<lable class="ui-opt-custom">+ 自定义</lable>';
				};

				$this.html(htmlStr);
			};

			if(opts.type === 'checkbox') {
				$this.delegate(".ui-opt-all", "click", function(){
					if($(this).hasClass('active')) {
						$this.find('.ui-opt').removeClass('active');
					} else {
						$this.find('.ui-opt').addClass('active');
					}
				});
				$this.delegate(".ui-opt", "click", function(){
					if($(this).hasClass('active')) {
						$(this).removeClass('active');
					} else {
						$(this).addClass('active');
					};
					if($this.find('span.active').length === ($this.find('span.ui-opt').length)) {
						$this.find('.ui-opt-all').addClass('active');
					} else {
						$this.find('.ui-opt-all').removeClass('active');
					}
				});
			} else {
				$this.delegate(".ui-opt", "click", function(){
					if($(this).hasClass('active')) {
						$(this).removeClass('active');
					} else {
						$(this).addClass('active').siblings().removeClass('active');
					}
				});
			};

			return {
				getValue: function() {
					var val = [];
					$this.find("span.active").each(function(i){
						val.push($(this).data('id'));
					});
					if(opts.type === 'radio') {
						val = val.join('');
					};
					return val;
				},
				setValue: function(arr) {
					$.each(arr, function(i, val){
						$this.find("[data-id=" + val +"]").addClass('active');
					});
					if($this.find('span.active').length === ($this.find('span.ui-opt').length)) {
						$this.find('.ui-opt-all').addClass('active');
					};
				}
			}
		};
	})();

	/**
	 * 分页插件
	 *
	 * @author Jie Chen (309558639@qq.com)
	 * @version 1.0
	 * @param {int} maxentries Number of entries to paginate
	 * @param {Object} opts Several options
	 * @return {Object} jQuery Object
	 */
	;(function($){
		/**
		 * @class Class for calculating pagination values
		 */
		$.PaginationCalculator = function(maxentries, opts) {
			this.maxentries = maxentries;
			this.opts = opts;
		};

		$.extend($.PaginationCalculator.prototype, {
			/**
			 * Calculate the maximum number of pages
			 * @method
			 * @returns {Number}
			 */
			numPages:function() {
				return Math.ceil(this.maxentries/this.opts.items_per_page);
			},
			/**
			 * Calculate start and end point of pagination links depending on
			 * current_page and num_display_entries.
			 * @returns {Array}
			 */
			getInterval:function(current_page)  {
				var ne_half = Math.floor(this.opts.num_display_entries/2);
				var np = this.numPages();
				var upper_limit = np - this.opts.num_display_entries;
				var start = current_page > ne_half ? Math.max( Math.min(current_page - ne_half, upper_limit), 0 ) : 0;
				var end = current_page > ne_half?Math.min(current_page+ne_half + (this.opts.num_display_entries % 2), np):Math.min(this.opts.num_display_entries, np);
				return {start:start, end:end};
			}
		});

		// Initialize jQuery object container for pagination renderers
		$.PaginationRenderers = {};

		/**
		 * @class Default renderer for rendering pagination links
		 */
		$.PaginationRenderers.defaultRenderer = function(maxentries, opts) {
			this.maxentries = maxentries;
			this.opts = opts;
			this.pc = new $.PaginationCalculator(maxentries, opts);
		};
		$.extend($.PaginationRenderers.defaultRenderer.prototype, {
			/**
			 * Helper function for generating a single link (or a span tag if it's the current page)
			 * @param {Number} page_id The page id for the new item
			 * @param {Number} current_page
			 * @param {Object} appendopts Options for the new item: text and classes
			 * @returns {jQuery} jQuery object containing the link
			 */
			createLink:function(page_id, current_page, appendopts){
				var lnk, np = this.pc.numPages();
				page_id = page_id<0?0:(page_id<np?page_id:np-1); // Normalize page id to sane value
				appendopts = $.extend({text:page_id+1, classes:""}, appendopts||{});
				if(page_id == current_page){
					lnk = $("<span class='current'>" + appendopts.text + "</span>");
				}
				else
				{
					lnk = $("<a>" + appendopts.text + "</a>")
						.attr('href', this.opts.link_to.replace(/__id__/,page_id));
				}
				if(appendopts.classes){ lnk.addClass(appendopts.classes); }
				if(appendopts.rel){ lnk.attr('rel', appendopts.rel); }
				lnk.data('page_id', page_id);
				return lnk;
			},
			// Generate a range of numeric links
			appendRange:function(container, current_page, start, end, opts) {
				var i;
				for(i=start; i<end; i++) {
					this.createLink(i, current_page, opts).appendTo(container);
				}
			},
			getLinks:function(current_page, eventHandler) {
				var begin, end,
					interval = this.pc.getInterval(current_page),
					np = this.pc.numPages(),
					fragment = $("<div class='pagination'></div>");

				// Generate "Previous"-Link
				if(this.opts.prev_text && (current_page > 0 || this.opts.prev_show_always)){
					fragment.append(this.createLink(current_page-1, current_page, {text:this.opts.prev_text, classes:"prev",rel:"prev"}));
				}
				// Generate starting points
				if (interval.start > 0 && this.opts.num_edge_entries > 0)
				{
					end = Math.min(this.opts.num_edge_entries, interval.start);
					this.appendRange(fragment, current_page, 0, end, {classes:'sp'});
					if(this.opts.num_edge_entries < interval.start && this.opts.ellipse_text)
					{
						$("<span>"+this.opts.ellipse_text+"</span>").appendTo(fragment);
					}
				}
				// Generate interval links
				this.appendRange(fragment, current_page, interval.start, interval.end);
				// Generate ending points
				if (interval.end < np && this.opts.num_edge_entries > 0)
				{
					if(np-this.opts.num_edge_entries > interval.end && this.opts.ellipse_text)
					{
						$("<span>"+this.opts.ellipse_text+"</span>").appendTo(fragment);
					}
					begin = Math.max(np-this.opts.num_edge_entries, interval.end);
					this.appendRange(fragment, current_page, begin, np, {classes:'ep'});

				}
				// Generate "Next"-Link
				if(this.opts.next_text && (current_page < np-1 || this.opts.next_show_always)){
					fragment.append(this.createLink(current_page+1, current_page, {text:this.opts.next_text, classes:"next",rel:"next"}));
				}
				$('a', fragment).click(eventHandler);
				return fragment;
			}
		});

		// Extend jQuery
		$.fn.pagination = function(maxentries, opts){

			// Initialize options with default values
			opts = $.extend({
				items_per_page:10,
				num_display_entries:11,
				current_page:0,
				num_edge_entries:0,
				link_to:"#",
				prev_text:"上一页",
				next_text:"下一页",
				ellipse_text:"...",
				prev_show_always:true,
				next_show_always:true,
				renderer:"defaultRenderer",
				show_if_single_page:true,
				load_first_page:false,
				callback:function(){return false;}
			},opts||{});

			var containers = this,
				renderer, links, current_page;

			/**
			 * This is the event handling function for the pagination links.
			 * @param {int} page_id The new page number
			 */
			function paginationClickHandler(evt){
				var links,
					new_current_page = $(evt.target).data('page_id'),
					continuePropagation = selectPage(new_current_page);
				if (!continuePropagation) {
					evt.stopPropagation();
				}
				return continuePropagation;
			}

			/**
			 * This is a utility function for the internal event handlers.
			 * It sets the new current page on the pagination container objects,
			 * generates a new HTMl fragment for the pagination links and calls
			 * the callback function.
			 */
			function selectPage(new_current_page) {
				// update the link display of a all containers
				containers.data('current_page', new_current_page);
				links = renderer.getLinks(new_current_page, paginationClickHandler);
				containers.empty();
				links.appendTo(containers);
				// call the callback and propagate the event if it does not return false
				var continuePropagation = opts.callback(new_current_page, containers);
				return continuePropagation;
			}

			// -----------------------------------
			// Initialize containers
			// -----------------------------------
			current_page = parseInt(opts.current_page, 10);
			containers.data('current_page', current_page);
			// Create a sane value for maxentries and items_per_page
			maxentries = (!maxentries || maxentries < 0)?1:maxentries;
			opts.items_per_page = (!opts.items_per_page || opts.items_per_page < 0)?1:opts.items_per_page;

			if(!$.PaginationRenderers[opts.renderer])
			{
				throw new ReferenceError("Pagination renderer '" + opts.renderer + "' was not found in jQuery.PaginationRenderers object.");
			}
			renderer = new $.PaginationRenderers[opts.renderer](maxentries, opts);

			// Attach control events to the DOM elements
			var pc = new $.PaginationCalculator(maxentries, opts);
			var np = pc.numPages();
			containers.off('setPage').on('setPage', {numPages:np}, function(evt, page_id) {
					if(page_id >= 0 && page_id < evt.data.numPages) {
						selectPage(page_id); return false;
					}
			});
			containers.off('prevPage').on('prevPage', function(evt){
					var current_page = $(this).data('current_page');
					if (current_page > 0) {
						selectPage(current_page - 1);
					}
					return false;
			});
			containers.off('nextPage').on('nextPage', {numPages:np}, function(evt){
					var current_page = $(this).data('current_page');
					if(current_page < evt.data.numPages - 1) {
						selectPage(current_page + 1);
					}
					return false;
			});
			containers.off('currentPage').on('currentPage', function(){
					var current_page = $(this).data('current_page');
					selectPage(current_page);
					return false;
			});

			// When all initialisation is done, draw the links
			links = renderer.getLinks(current_page, paginationClickHandler);
			containers.empty();
			if(np > 1 || opts.show_if_single_page) {
				links.appendTo(containers);
			}
			// call callback function
			if(opts.load_first_page) {
				opts.callback(current_page, containers);
			}
		}; // End of $.fn.pagination block

	})(jQuery);


	//通用表格
	;(function($, window, document, undefined) {
	  	var name = "niceTable",
	      	instance = null,
	      	defaults = {
	      		perPage: 20, //每页显示条数
	      		midRange: 5,
	      		edgeRange: 2
	      	};

		function Plugin(element, options) {
			this.options = $.extend({}, defaults, options);
			this.$window = $(window);
			this.$document = $(document);
			this.$container = $(element);
			this.$table = this.$container.find('.ui-table');
			this.$paging = this.$container.find('.ui-paging');
			this.init();
		};

		Plugin.prototype = {

			constructor : Plugin,

			init: function() {
				this.render();
				this.paging();
			  	this.eventHandler();
			},

			render: function(){

			},

			paging: function() {
				var opts = this.options;
	            this.$paging.pagination(opts.records, {
	                num_edge_entries: opts.edgeRange,
	                num_display_entries: opts.midRange,
	                //callback: pageselectCallback,
	                items_per_page: opts.perPage
	            });
			},

			reloadGrid: function(){

			},

			eventHandler: function(){
				var _this = this;
	            this.$container.delegate(".ckb-all", "click", function(e){
	                if(this.checked) {
	                    _this.$container.find('.ckb').prop("checked", true);
	                } else {
	                    _this.$container.find('.ckb').prop("checked", false);
	                };
	            });

	            this.$container.delegate(".ckb", "click", function(e){
	                var $ckb = _this.$container.find('.ckb');
	                if(this.checked){
	                    var $checked = _this.$container.find('.ckb:checked');
	                    if($ckb.length === $checked.length) {
	                        _this.$container.find('.ckb-all').prop("checked", true);
	                    };
	                } else {
	                    _this.$container.find('.ckb-all').prop("checked", false);
	                };
	            });
			},

			addRow: function(){

			}


		};

		$.fn[name] = function(arg) {
			var type = $.type(arg);

			if (type === "object") {
			  	if (this.length && !$.data(this, name)) {
			    	instance = new Plugin(this, arg);
			    	this.data(name, instance);
			  	};
			  	return instance;
			};

			if (arg === "destroy") {
			  instance.destroy();
			  this.each(function() {
			    $.removeData(this, name);
			  });
			  return this;
			}

			if (type === "string") {

			  	return this;
			}

			if (type === 'number' && arg % 1 === 0) {
			  if (instance.validNewPage(arg)) instance.paginate(arg);
			  return this;
			}

			return this;
		};

		$.fn.tableNice = function(config){
			var defaults = {
				isAll: false,
				type: 'radio',
				extend: false
			};
			var opts = $.extend({}, defaults, config);


			return {
				addRow: function() {

				},
				editRow: function() {

				},
				deleteRow: function(){

				}
			}
		};
	})(jQuery, window, document);

	//通用省市区学校选择
	;(function(){
		$.fn.areaBox = function(opts) {
	        var htmlStr =
	            '   <div class="select-frame">'+
	            '       <div class="point">省</div>'+
	            '       <div class="select-list province ui-scroll"></div>'+
	            '   </div>'+
	            '   <div class="select-frame">'+
	            '       <div class="point">市</div>'+
	            '       <div class="select-list city ui-scroll">'+
	            '       </div>'+
	            '   </div>'+
	            '   <div class="select-frame">'+
	            '       <div class="point">区/县</div>'+
	            '       <div class="select-list area ui-scroll">'+
	            '       </div>'+
	            '   </div>'+
	            '   <div class="select-frame">'+
	            '       <div class="point">学校</div>'+
	            '       <div class="select-list school ui-scroll">'+
	            '       </div>'+
	            '   </div>'+
	            '   <div class="result-section">'+
	            '       <div class="point">已选择</div>'+
	            '       <div class="select-list selected-list ui-scroll">'+
	            '       </div>'+
	            '   </div>';

			var defaults = {
			};
			var opts = $.extend({}, defaults, opts);
			function areaRangeInit(data,obj){
			 	var provinceHtml = '<div class="options sel-all" data-key="0"><span class="name">全部省份</span></div>';
                var provinceList = data.provinceList;
                $.each(provinceList, function(key, val){
                    provinceHtml += '<div class="options" data-key="' + key + '"><span class="name">' + val.mc+ '</span></div>';
                });
                obj.find('.province').html(provinceHtml);
			}
			this.each(function() {
				var $this = $(this);
				var $areaBox = $(this);
				var selectedHtml = '';
				var cityList = null,
					regionList = null;
				$this.html(htmlStr);
				if(SYSTEM.areaRange){
               		areaRangeInit(SYSTEM.areaRange,$this);
               		cityList = SYSTEM.areaRange.cityList;
                	regionList = SYSTEM.areaRange.regionList;
				}else{
					Public.get({
		               	loading:false,
		                url: SYSTEM.baseUrl + 'basedata/serverCommon/getUserAreasCode?typeId='+SYSTEM.typeId,
		                success: function(data) {
		           			SYSTEM.areaRange = data.data;
                    		areaRangeInit(SYSTEM.areaRange,$this);
                    		cityList = SYSTEM.areaRange.cityList;
                			regionList = SYSTEM.areaRange.regionList;
		                }
		            });
				}
				//载入已选项
				if(opts.value) {
					$.each(opts.value, function(key, val){
                        selectedHtml += '<div class="options" data-key="' + val.id + '" data-type="' + val.type + '"><span class="name">' + val.name+ '</span><span class="spot">移除</span></div>';
                    });
                    $this.find('.selected-list').html(selectedHtml);
				};

                //单个省份
                $this.delegate(".province .options:gt(0)","click",function(){
                    var $this = $(this);
                    var $selectedList = $areaBox.find('.selected-list');
                    if($this.hasClass('selected')) {
                        $(this).removeClass('selected').siblings().removeClass('selected');
                        $areaBox.find('.city').empty();
                        $areaBox.find('.area').empty();
                        $areaBox.find('.school').empty();
                        $selectedList.find('[data-type=all]').remove();
                    } else {
                        var cityHtml = '<div class="options sel-all" data-key="0"><span class="name">全部市区</span></div>';
                        var parentKey = $(this).data('key');
                        $(this).addClass('selected').siblings().removeClass('selected');
                        $.each(cityList, function(key, val){
                            if(val.parent == parentKey) {
                                cityHtml += '<div class="options" data-key="' + key + '"><span class="name">' + val.mc+ '</span></div>';
                            };
                        });
                        $('.city').html(cityHtml);
                        $areaBox.find('.area').empty();
                        $areaBox.find('.school').empty();
                    }
                });
                //单个市区
                $this.delegate(".city .options:gt(0)","click",function(){
                    var $this = $(this);
                    var $selectedList = $areaBox.find('.selected-list');
                    var $province = $areaBox.find('.province .selected');
                    if($this.hasClass('selected')) {
                        $(this).removeClass('selected').siblings().removeClass('selected');
                        $areaBox.find('.area').empty();
                        $areaBox.find('.school').empty();
                        $selectedList.find('[data-key=' + $province.data('key') + ']').remove();
                    } else {
                        var regionHtml = '<div class="options sel-all" data-key="0"><span class="name">全部区域</span></div>';
                        var parentKey = $(this).data('key');
                        $(this).addClass('selected').siblings().removeClass('selected');
                        $.each(regionList, function(key, val){
                            if(val.parent == parentKey) {
                                regionHtml += '<div class="options" data-key="' + key + '"><span class="name">' + val.mc+ '</span></div>';
                            };
                        });
                        $('.area').html(regionHtml);
                        $areaBox.find('.school').empty();
                    }
                });
                //单个区域
                $this.delegate(".area .options:gt(0)","click",function(){
                    var $this = $(this);
                    var $selectedList = $areaBox.find('.selected-list');
                    var $city = $areaBox.find('.city .selected');
                    if($this.hasClass('selected')) {
                        $(this).removeClass('selected').siblings().removeClass('selected');
                        $areaBox.find('.school').empty();
                        $selectedList.find('[data-key=' + $city.data('key') + ']').remove();
                    } else {
                    	var parentKey = $(this).data('key');
	                    $(this).addClass('selected').siblings().removeClass('selected');
                        var urlStr = SYSTEM.baseUrl +"basedata/serverCommon/getAreaSchools?areaCode=" + parentKey;
	                    //ajax获取学校
	                    $.getJSON(urlStr, function(data){
	                    	//console.log(data)
	                        var schoolHtml = '<div class="options sel-all" data-key="0"><span class="name">全部学校</span></div>';
	                        $.each(data.data.schoolList, function(key, val){
	                            schoolHtml += '<div class="options" data-area="' + parentKey + '" data-key="' + val.id + '"><span class="name">' + val.name+ '</span></div>';
	                        });
	                        $('.school').html(schoolHtml);
	                    });
                    }
                });

                //单个学校
                $this.delegate(".school .options:gt(0)","click",function(){
                    var $this = $(this);
                    var $selected = $this;
                    var $selectedList = $areaBox.find('.selected-list');
                    var area = $areaBox.find('.area .selected').data('key');
                    if($this.hasClass('selected')) {
                    	if($this.parent().find('.sel-all').hasClass('selected')) {
                    		$(this).removeClass('selected').siblings().removeClass('selected');
                    		$selectedList.find('[data-key=' + area + ']').remove();
                    	} else {
                        	$this.removeClass('selected');
                        	$selectedList.find('[data-key=' + $selected.data('key') + ']').remove();
                    	};
                    } else {
	                    $this.addClass('selected');
	                    var key = $selected.data('key');
                        var name = $selected.find('.name').text();
                        var area = $selected.data('area');
                        if($selectedList.find('[data-key=' + $selected.data('key') + ']').length < 1) {
                            $selectedList.append('<div class="options" data-area="' + area + '" data-key="' + key + '" data-type="school"><span class="name">' + name + '</span><span class="spot">移除</span></div>');
                        };
                    }
                });

                //全部省份
                $this.delegate(".province .sel-all","click",function(){
                    var $this = $(this);
                    $areaBox.find('.city').empty();
                    $areaBox.find('.area').empty();
                    $areaBox.find('.school').empty();
                    $areaBox.find('.selected-list').empty();
                    if($this.hasClass('selected')) {
                        $this.removeClass('selected').siblings().removeClass('selected');
                    } else {
                        $this.addClass('selected').siblings().addClass('selected');
                        $areaBox.find('.selected-list').html('<div class="options" data-key="0" data-type="all"><span class="name">全部省份</span><span class="spot">移除</span></div>');
                    }
                });

                //全部市区
                $this.delegate(".city .sel-all","click",function(){
                    var $this = $(this);
                    var $selected = $areaBox.find('.province .selected');
                    var $selectedList = $areaBox.find('.selected-list');
                        $areaBox.find('.area').empty();
                        $areaBox.find('.school').empty();
                    if($this.hasClass('selected')) {
                        $selectedList.find('[data-key=' + $selected.data('key') + ']').remove();
                        $this.removeClass('selected').siblings().removeClass('selected');
                    } else {
                        var key = $selected.data('key');
                        var name = $selected.find('.name').text();
                        $this.addClass('selected').siblings().addClass('selected');
                        var mark = String(key).slice(0, 2);
                        $selectedList.find('[data-key^=' + mark + ']').remove();
                        if($selectedList.find('[data-key=' + $selected.data('key') + ']').length < 1) {
                            $selectedList.append('<div class="options" data-key="' + key + '" data-type="province"><span class="name">' + name + '</span><span class="spot">移除</span></div>');
                        };

                    }
                });

                //全部区域
                $this.delegate(".area .sel-all","click",function(){
                    var $this = $(this);
                    var $selected = $areaBox.find('.city .selected');
                    var $selectedList = $areaBox.find('.selected-list');
                    $areaBox.find('.school').empty();
                    if($this.hasClass('selected')) {
                        $selectedList.find('[data-key=' + $selected.data('key') + ']').remove();
                        $this.removeClass('selected').siblings().removeClass('selected');
                    } else {
                        var key = $selected.data('key');
                        var name = $selected.find('.name').text();
                        var mark = String(key).slice(0, 4);
                        $selectedList.find('[data-key^=' + mark + ']').remove();
                        $this.addClass('selected').siblings().addClass('selected');
                        if($selectedList.find('[data-key=' + $selected.data('key') + ']').length < 1) {
                            $selectedList.append('<div class="options" data-key="' + key + '" data-type="city"><span class="name">' + name + '</span><span class="spot">移除</span></div>');
                        };
                    }
                });

                //全部学校
                $this.delegate(".school .sel-all","click",function(){
                    var $this = $(this);
                    var $selected = $areaBox.find('.area .selected');
                    var $selectedList = $areaBox.find('.selected-list');
                    if($this.hasClass('selected')) {
                        $selectedList.find('[data-key=' + $selected.data('key') + ']').remove();
                        $this.removeClass('selected').siblings().removeClass('selected');
                    } else {
                        var key = $selected.data('key');
                        var name = $selected.find('.name').text();
                        $this.addClass('selected').siblings().addClass('selected');
                        if($selectedList.find('[data-key=' + $selected.data('key') + ']').length < 1) {
                            $selectedList.append('<div class="options" data-key="' + key + '" data-type="area"><span class="name">' + name + '</span><span class="spot">移除</span></div>');
                        	$selectedList.find('[data-area=' + key + ']').remove();
                        };
                    }
                });

                //移除
                $this.delegate(".selected-list .options","click",function(){
                    var $this = $(this);
                    var key = $this.data('key');
                    var name = $this.find('.name').text();
                    var type = $this.data('type');
					switch (type) {
					case 'area':
					    $areaBox.find('.school').find('.options').removeClass('selected');
					    break;
					case 'city':
					    $areaBox.find('.area').find('.options').removeClass('selected');
					    break;
					case 'province':
					    $areaBox.find('.city').find('.options').removeClass('selected');
					    break;
					case 'all':
					    $areaBox.find('.province').find('.options').removeClass('selected');
					    break;
					default:
					    $areaBox.find('.school').find('[data-key=' + key + ']').removeClass('selected');
					};
                    $this.remove();
                });
			});
		}
	})();

	//通用双向选择
	;(function(){
		$.fn.multipleBox = function(config) {
	        var htmlStr =
	            '   <div class="select-frame">'+
	            '       <div class="point">组织机构</div>'+
	            '       <div class="select-list org ui-scroll">'+
	            '       </div>'+
	            '   </div>'+
	            '   <div class="result-section">'+
	            '       <div class="point">已选择</div>'+
	            '       <div class="select-list selected-list ui-scroll">'+
	            '       </div>'+
	            '   </div>';

			var defaults = {
			};
			var opts = $.extend({}, defaults, config);
			var $multipleBox = this;
			this.each(function() {
				var optionHtml = '';
				var selectedHtml = '';
				$(this).html(htmlStr);
				(function(data){
                    $.each(data, function(i, val){
                        optionHtml += '<div class="options" data-id="' + val.orgId + '" data-type="' + val.orgTypeName + '" data-grade="' + val.gradeNames + '" data-lesson="' + val.lessonNames + '"><span class="name">' + val.orgName+ '</span></div>';
                    });
                    $multipleBox.find('.org').html(optionHtml);
				})(opts.data);

                //单个选择
                $multipleBox.delegate(".select-list .options", "click", function(){
                    var $this = $(this);
                    var $selected = $this;
                    var $selectedList = $multipleBox.find('.selected-list');
                    if($this.hasClass('selected')) {
                        $this.removeClass('selected');
                        $selectedList.find('[data-id=' + $selected.data('id') + ']').remove();
                    } else {
	                    $this.addClass('selected');
	                    var id = $selected.data('id');
                        var name = $selected.find('.name').text();
                        if($selectedList.find('[data-id=' + $selected.data('id') + ']').length < 1) {
                            $selectedList.append('<div class="options" data-id="' + id + '"><span class="name">' + name + '</span><span class="spot">移除</span></div>');
                        };
                    }
                });

                //移除
                $multipleBox.delegate(".selected-list .options","click",function(){
                    var $this = $(this);
                    var id = $this.data('id');
                    var name = $this.find('.name').text();
                    $multipleBox.find('.org').find('[data-id=' + id + ']').removeClass('selected');
                    $this.remove();
                });
			});
		}
	})();
});