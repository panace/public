<?php

namespace app\modules\admin\models;

/**
 * This is the ActiveQuery class for [[TbAdmin]].
 *
 * @see TbAdmin
 */
class AdminQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TbAdmin[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TbAdmin|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
