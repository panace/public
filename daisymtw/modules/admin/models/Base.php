<?php

namespace app\modules\admin\models;

use Yii;

class Base extends \yii\db\ActiveRecord
{
   public function beforeSave($insert)
    {
        if($insert){
            $this->creationtime = $this->creationtime ? $this->creationtime : date('Y-m-d H:i:s');
        }
        $this->updatetime = $this->updatetime ? $this->updatetime : date('Y-m-d H:i:s');
        $this->deleted = $this->deleted ? $this->deleted : 0;
        return parent::beforeSave($insert);
    }
}
