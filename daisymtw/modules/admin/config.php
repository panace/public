<?php

$config = [
    'id' => 'admin', 
    'components' => [
        
        'user' => [  // Webuser for the admin area (admin)
            'class'             => 'yii\web\User',
            'loginUrl'          => ['/admin/default/login/'],
            'identityClass'     => 'app\modules\admin\models\AdminIdentify',
            'idParam'           => '__adminid',
            // 'identityCookie'    => ['name'=>'_aa','httpOnly' => true],
        ],
    ],
];

return $config;
