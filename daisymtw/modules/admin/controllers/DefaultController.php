<?php

namespace app\modules\admin\controllers;
use Yii;
use yii\web\Controller;
use app\modules\admin\models\AdminLoginForm;
/**
 * Default controller for the `admin` module
 */
class DefaultController extends BaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {   
        // mlog(Yii::$app->basePath);
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {

        if (!Yii::$app->getModule('admin')->user->isGuest) {
            return $this->goHome();
        }

        $model = new AdminLoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
        	var_dump(Yii::$app->getModule('admin')->user->id);exit;
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }
}
