<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadForm;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for User model.
 */
class BaseController extends Controller
{
    function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
        $this->layout='@app/modules/admin/views/layouts/main.php';
    }

    public function actions()
	{
	    return [
	        'Kupload' => [
	            'class' => 'pjkui\kindeditor\KindEditorAction',
	        ]
	    ];
	}

	public function actionUpload()
	{
		$path = '';
		$UploadModel = new UploadForm();
        $UploadModel->imageFile = UploadedFile::getInstanceByName('UploadedFile');
        $path = $UploadModel->upload();
        return $path;
	}
}
