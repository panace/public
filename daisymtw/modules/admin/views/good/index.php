<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\db\GoodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', '商品管理');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="good-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="container-fluid fix">
        <?= Html::a(Yii::t('app', '创建商品'), ['create'], ['class' => 'ui-btn ui-btn-primary pull-right']) ?>
    </div>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'], 
            'id',
            'name',
            'category_id',
            //'goods_no',
            'unit',
            //'content:ntext',
            //'img',
            [
                'label'=>'封面图',
                'format'=>'raw',
                'value'=>function($model){
                    return Html::img($model->img,
                                ['class' => 'img-circle',
                                'width' => 50]
                    );
                }
            ],
            //'imgs:ntext',
            'sell_price',
            'market_price',
            'store_nums',
            //'desc',
            //'weight',
            'specs:ntext',
            'attrs:ntext',
            //'status',
            //'creationtime',
            //'updatetime', 
            [
                'label'=>'状态',  
                'attribute' => 'deleted',  
                'value' => function ($model) {
                    $state = [
                        '0' => '未删除',
                        '1' => '已删除',
                    ];
                 return $state[$model->deleted];
                },
                'headerOptions' => ['width' => '60'] 
            ],
            //['class' => 'app\modules\admin\components\AdminActionColumn'], 
            [
               //动作列yii\grid\ActionColumn 
               //用于显示一些动作按钮，如每一行的更新、删除操作。
              'class' => 'app\modules\admin\components\AdminActionColumn',
              'header' => '操作', 
              'template' => '{view} {update} {delete}',//只需要展示删除和更新
              'headerOptions' => ['width' => '100'],
              'buttons' => [
                'delete' => function($url, $model, $key){
                   return Html::a('<i class="fa fa-ban"></i> 删除',
                        ['del', 'id' => $key], 
                        [
                         'class' => 'btn btn-default btn-xs',
                         'data' => ['confirm' => '你确定要删除文章吗？',]
                        ]
                   );
                 },                     
               ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
