<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\db\Good */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="good-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->textInput() ?>

    <?= $form->field($model, 'tag_id')->textInput() ?>

    <?= $form->field($model, 'goods_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <!-- <?= $form->field($model, 'img')->textInput(['maxlength' => true]) ?> -->
    <?= $form->field($model, 'img')->fileInput() ?>

    <?= $form->field($model, 'imgs')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sell_price')->textInput(['maxlength' => true,'value'=>isset($model['sell_price'])? $model['sell_price']: 0]) ?>

    <?= $form->field($model, 'market_price')->textInput(['maxlength' => true,'value'=>isset($model['market_price'])? $model['market_price']: 0]) ?>

    <?= $form->field($model, 'store_nums')->textInput(['value'=>isset($model['store_nums'])? $model['store_nums']: 0]) ?>

    <?= $form->field($model, 'desc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <?= $form->field($model, 'specs')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'attrs')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textInput(['value'=>isset($model['status'])? $model['status']: 0]) ?>

    <?= $form->field($model, 'creationtime')->textInput() ?>

    <?= $form->field($model, 'updatetime')->textInput() ?>

    <?= $form->field($model, 'deleted')->textInput(['value'=>isset($model['deleted'])? $model['deleted']: 0]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
