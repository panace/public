<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\Good */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Good',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Goods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id, 'name' => $model->name]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="good-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
