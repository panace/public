
<!DOCTYPE html>
<html> 
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <title>美瞳网后台管理</title>
    <link rel="stylesheet" href="/public/css/common.css">
	<link rel="stylesheet" href="/public/css/ui.jqgrid.css">
	<link rel="stylesheet" href="/public/css/ui-dialog.css">
	<link rel="stylesheet" href="/public/css/pikaday.css">
	<link rel="stylesheet" href="/public/css/webuploader.css">
	<link rel="stylesheet" href="/public/css/nprogress.css">
	<link rel="stylesheet" href="/public/css/ui.css">
	<link rel="stylesheet" href="/public/css/layout.css">
	<link rel="stylesheet" href="/public/css/basic.css">
	<link rel="stylesheet" href="/public/css/lib.css"> 
	<link rel="stylesheet" href="/public/css/bootstrap.min.css">
    <script type="text/javascript" src="/public/js/lib/jquery-1.12.3.js"></script> 
</head>
<body>

	<div class="container" id="Spa">
	<div class="header fix">
    	<h1 class="l logo"><i class="iconfont baseDate"></i>美瞳网后台管理</h1>
        <div class="r nav">
        	<ul>
            	<li>
                	<a href="">OMS首页</a>
                </li>
                <li class="line">|</li>
                <li><label>关于</label></li>
                <!--
            	<li class="dropdown">
                	<a href="#">何挺<i class="ic-delta-box"><i class="ic-delta ic-delta-a"></i><i class="ic-delta ic-delta-b"></i></i></a>
                    <div class="dropdown-menu">
                    	<dl>
                        	<dd><a href="#">修改密码</a></dd>
                            <dd><a href="#">退出</a></dd>
                        </dl>
                    </div>
                </li>
                -->
            </ul>
        </div>
    </div>
    <div class="content">
    	<div class="sidebar">
        	<div class="side-bd">               
                <dl> 
                   	<dd><a href="/admin/manage">后台用户管理</a></dd>
					<dd><a href="/admin/user">前台台用户管理</a></dd>
					<dd><a href="/admin/goods-category">商品分类管理</a></dd>
					<dd><a href="/admin/good">商品管理</a></dd>
               </dl>
            </div>
        </div>
        <div class="main" id="pageMain">
        	<div class="row-fluid p10">
				<?php 
					/* @var $this \yii\web\View */
					/* @var $content string */ 
					use yii\helpers\Html;
					use yii\bootstrap\Nav;
					use yii\bootstrap\NavBar;
					use yii\widgets\Breadcrumbs;
					use app\assets\AppAsset; 
					AppAsset::register($this);
				?> 
				<?= Breadcrumbs::widget([ 'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?> 

				<?= $content ?>
			</div> 
        	
        </div>
    </div>
</div>
	<div class="mainBox" style=" display: none; overflow:hidden;">
		<?php include("header.php"); ?>  
		<div class="container-fluid">
			<div class="row-fluid">
			    <div class="span2" style="width: 180px; float: left;">
			      <?php include("sidebar.php"); ?> 
			    </div>
			    <div class="span10" style="margin-left: 190px;">
			    	
			    	<div class="table-responsive">
			    		
			    	</div> 
			    </div>
		  	</div> 
		</div> 
		<?php include("footer.php"); ?> 

	</div> 
</body> 
</html>