<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\AdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', '用户管理中心');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-index"> 
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php  // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="container-fluid fix">
        <?= Html::a(Yii::t('app', '创建账号'), ['create'], ['class' => 'ui-btn ui-btn-primary pull-right ']) ?>
    </div>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'], 
            'uid',
            'username',
            'password',
            'name',
            'mobile',
            // 'mail',
            // 'qq',
            // 'logo',r
            // 'lastlogintime',
            // 'creationtime',
            // 'updatetime',
            // 'deleted',

            ['class' => 'app\modules\admin\components\AdminActionColumn'], 
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
