<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\db\GoodsCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', '商品分类管理');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goods-category-index">

    <h1><?=  Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="container-fluid">
        <?= Html::a(Yii::t('app', '创建商品分类'), ['create'], ['class' => 'btn btn-success pull-right']) ?>
    </div>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'alias',
            'parent_id',
            'logo',
            // 'sort',
            // 'describe',

            ['class' => 'app\modules\admin\components\AdminActionColumn'], 
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
